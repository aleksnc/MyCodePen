let express = require("express");
let fs =require('fs');
let app = express();
var bodyParser = require('body-parser');

var urlencodedParser = bodyParser.urlencoded({extended: false});

app.all('/', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.get("/", function(request, response){

    response.send("<h2>Привет Express!</h2>");
});

app.post("/post", urlencodedParser, function (request, response) {
    fs.writeFile("./app/components/Hello.tsx", request.body.foo, function(err) {
        if(err) {
            return console.log(err);
        }

    });


    console.log(request.body) //you will get your data in this as object.
})

app.listen(3000);