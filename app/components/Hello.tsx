import * as React from "react";


export interface HelloProps {
    compiler: string;
    framework: string;
}

export const Hello = (props: HelloProps) => <h2 >Hserllo from {props.compiler} and {props.framework}!</h2>;