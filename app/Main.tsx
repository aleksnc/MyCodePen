import * as React from "react";

import AceEditor from 'react-ace';
import {Hello} from "./components/Hello";

import 'brace/mode/typescript';
import 'brace/theme/monokai';

export class Main extends React.Component<{}> {

    state = {
        data: '',
    }


    myLoad = () => {
        var file = new Blob([this.state.data], {type: 'text/plain'});
        if (window.navigator.msSaveOrOpenBlob) // IE10+
            window.navigator.msSaveOrOpenBlob(file, 'Hello.tsx');
        else { // Others
            var a = document.createElement("a"),
                url = URL.createObjectURL(file);
            a.href = url;
            a.download = 'test.tsx';
            document.body.appendChild(a);
            a.click();
            setTimeout(function () {
                document.body.removeChild(a);
                window.URL.revokeObjectURL(url);
            }, 0);
        }
    }

    onChange =(e:string)=>{
        console.log(e);
        this.setState({
            data:e
        })

        console.log(this.state.data);
    }

    myPost = () => {
        let main =this;
        console.log(main.state.data);
        fetch('http://127.0.0.1:3000/post', {
            method: 'post',
            headers: {
                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
            },
            body: 'foo='+ main.state.data
        }).then(function(response) {
            console.log(response);
        }).catch(function(err) {
            console.log(err);
        });

    }

    componentDidMount() {
        let main = this;

        fetch('/app/components/Hello.tsx')
            .then(function (response) {
                response.text().then(function (data) {
                    main.setState({
                        data
                    })
                });
            })
            .catch(function (err) {
                console.log('Fetch Error :-S', err);
            });
    }

    render() {
        return (
            <div>
                <Hello compiler="sdsa" framework="React"/>
                <AceEditor
                    mode="typescript"
                    theme="monokai"
                    name="blah2"
                    fontSize={14}
                    showPrintMargin={false}
                    showGutter={true}
                    highlightActiveLine={false}
                    value={this.state.data}
                    onChange={e=>this.onChange(e)}
                />
                <button onClick={this.myLoad}>Download Hello file</button>
                <br />
                <br />
                <button onClick={this.myPost}>Save change</button>
            </div>
        );
    }
}